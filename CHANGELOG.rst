CHANGELOG
=========

This project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[1.3.0] - 2022-09-07
  - Fixed DCBusXperm issues in some modules
  - Added DCB ConfigRegRW and WireROSe modules
  - Added some commonly used functions in CommonFunc.bsv
  
[1.2.0] - 2022-09-06
  - Added custom DCB registers with WC
  - Added wrap around counter module.
  - Added ug sized bypass fifo.

[1.1.0] - 2022-07-08
  - Added register overrides for asyn versions.
  - Fixed imports and CReg instance.
  - Moved comment to header and added reg overrides to all files.

[1.0.0] - 2022-07-08
  - initial tagged release
