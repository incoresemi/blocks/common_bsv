package CommonFunc;
  
/*doc: function: This function reverses endianess of a n-bit value.*/
function Bit#(n) reverse_endian(Bit#(n) inp) provisos(Div#(n,8,nby8), Add#(z__, 8, n));
  let v_n= valueOf(n);
  let v_nby8= valueOf(nby8);
  Bit#(n) rev_inp= 0;
  for(Integer i=0; i<v_nby8; i=i+1) begin
    Bit#(8) one_byte= inp[(i*8)+7 : (i*8)];
    rev_inp[v_n-1-(i*8):v_n-8-(i*8)] = one_byte;
  end
  return rev_inp;
endfunction

/*doc: function: Rotate Right*/
function Bit#(m) rotr (Bit#(m) x,Integer n) provisos(Add#(m,m,m2));
  Bit#(m2) y={x,'d0} >> n;
  return y[valueOf(m2)-1:valueOf(m)]|y[valueOf(m)-1:0];
endfunction:rotr

/*doc: function: Rotate Left*/
function Bit#(m) rotl (Bit#(m) x,Integer n) provisos(Add#(m,m,m2));
  Bit#(m2) y={'d0,x} << n;
  return y[valueOf(m2)-1:valueOf(m)]|y[valueOf(m)-1:0];
endfunction:leftrotate

endpackage