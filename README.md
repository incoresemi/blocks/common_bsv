
**⚠ WARNING: This project has been moved to [new location](https://gitlab.incoresemi.com/blocks/common_bsv.git). It will soon be archived and eventually deleted.**

## Common BSV Modules

This repo contains some of the common and generic building blocks that 
can be used across BSV designs.
