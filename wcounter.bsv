/*
  Copyright (c) 2021 Incore Semiconductors Pvt. Ltd. All Rights Reserved. See LICENSE.incore for more details.
  Created On:  Fri Jun 11, 2021 19:19:31 
  Author(s): S Pawan Kumar <pawan.kumar@incoresemi.com> gitlab: @pawks github: @pawks
*/

package wcounter;
  import FIFOF        :: * ;
  import Vector       :: * ;
  import SpecialFIFOs :: * ;
  import FIFOF        :: * ;

  interface Ifc_wcounter_u#(numeric type t);
    method Action inc;
    method Bit#(t) value;
    method Action reset;
  endinterface: Ifc_wcounter_u
  module mkwcounter_u#(Integer init, Integer max)(Ifc_wcounter_u#(t));
    /*doc:reg: */
    Reg#(Bit#(t)) rg_count[2] <- mkCReg(2, fromInteger(init));
    method Action inc;
      if (valueOf(TExp#(t)) == max || (rg_count[0] != fromInteger(max-1)))
        rg_count[0] <= rg_count[0] + 1;
      else
        rg_count[0] <= 0;
    endmethod: inc
    method value = rg_count[0];
    method Action reset;
      rg_count[1] <= 0;
    endmethod:reset
  endmodule: mkwcounter_u

  interface Ifc_wcounter_d#(numeric type t);
    method Action dec;
    method Bit#(t) value;
    method Action reset;
  endinterface: Ifc_wcounter_d
  module mkwcounter_d#(Integer init, Integer max)(Ifc_wcounter_d#(t));
    /*doc:reg: */
    Reg#(Bit#(t)) rg_count[2] <- mkCReg(2, fromInteger(init));
    method Action dec;
      if (valueOf(TExp#(t)) == max || (rg_count[0] != fromInteger(0)))
        rg_count[0] <= rg_count[0] - 1;
      else
        rg_count[0] <= fromInteger(max-1);
    endmethod: dec
    method value = rg_count[0];
    method Action reset;
      rg_count[1] <= fromInteger(max);
    endmethod:reset
  endmodule: mkwcounter_d

  interface Ifc_wcounter_ud#(numeric type t);
    method Action inc;
    method Action dec;
    method Bit#(t) value;
    method Action reset;
  endinterface: Ifc_wcounter_ud
  module mkwcounter_ud#(Integer init, Integer max)(Ifc_wcounter_ud#(t));
    /*doc:reg: */
    Reg#(Bit#(t)) rg_count[3] <- mkCReg(3, fromInteger(init));
    method Action dec;
      if (valueOf(TExp#(t)) == max || (rg_count[0] != fromInteger(0)))
        rg_count[0] <= rg_count[0] - 1;
      else
        rg_count[0] <= fromInteger(max-1);
    endmethod: dec
    method Action inc;
      if (valueOf(TExp#(t)) == max || (rg_count[1] != fromInteger(max-1)))
        rg_count[1] <= rg_count[1] + 1;
      else
        rg_count[1] <= 0;
    endmethod: inc
    method value = rg_count[0];
    method Action reset;
      rg_count[2] <= fromInteger(0);
    endmethod:reset
  endmodule: mkwcounter_ud

  interface Ifc_wcounter_udr#(numeric type t);
    method ActionValue#(Bit#(t)) inc;
    method ActionValue#(Bit#(t)) dec;
    method Bit#(t) value;
    method Action reset;
  endinterface: Ifc_wcounter_udr
  module mkwcounter_udr#(Integer init, Integer max)(Ifc_wcounter_udr#(t));
    /*doc:reg: */
    Reg#(Bit#(t)) rg_count[3] <- mkCReg(3, fromInteger(init));
    method ActionValue#(Bit#(t)) dec;
      Bit#(t) lv_val = ?;
      if (valueOf(TExp#(t)) == max || (rg_count[0] != fromInteger(0)))
        lv_val = rg_count[0] - 1;
      else
        lv_val = fromInteger(max-1);
      rg_count[0] <= lv_val;
      return lv_val;
    endmethod: dec
    method ActionValue#(Bit#(t)) inc;
      Bit#(t) lv_val = ?;
      if (valueOf(TExp#(t)) == max || (rg_count[1] != fromInteger(max-1)))
        lv_val = rg_count[1] + 1;
      else
        lv_val = 0;
        rg_count[1] <= lv_val;
      return lv_val;
    endmethod: inc
    method value = rg_count[0];
    method Action reset;
      rg_count[2] <= fromInteger(0);
    endmethod:reset
  endmodule: mkwcounter_udr

endpackage: wcounter
